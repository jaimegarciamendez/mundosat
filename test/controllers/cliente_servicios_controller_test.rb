require 'test_helper'

class ClienteServiciosControllerTest < ActionDispatch::IntegrationTest
  setup do
    @cliente_servicio = cliente_servicios(:one)
  end

  test "should get index" do
    get cliente_servicios_url
    assert_response :success
  end

  test "should get new" do
    get new_cliente_servicio_url
    assert_response :success
  end

  test "should create cliente_servicio" do
    assert_difference('ClienteServicio.count') do
      post cliente_servicios_url, params: { cliente_servicio: { cliente_id: @cliente_servicio.cliente_id, direccion: @cliente_servicio.direccion, estado: @cliente_servicio.estado, fecha_alta: @cliente_servicio.fecha_alta, fecha_baja: @cliente_servicio.fecha_baja, motivo: @cliente_servicio.motivo, servicio_id: @cliente_servicio.servicio_id } }
    end

    assert_redirected_to cliente_servicio_url(ClienteServicio.last)
  end

  test "should show cliente_servicio" do
    get cliente_servicio_url(@cliente_servicio)
    assert_response :success
  end

  test "should get edit" do
    get edit_cliente_servicio_url(@cliente_servicio)
    assert_response :success
  end

  test "should update cliente_servicio" do
    patch cliente_servicio_url(@cliente_servicio), params: { cliente_servicio: { cliente_id: @cliente_servicio.cliente_id, direccion: @cliente_servicio.direccion, estado: @cliente_servicio.estado, fecha_alta: @cliente_servicio.fecha_alta, fecha_baja: @cliente_servicio.fecha_baja, motivo: @cliente_servicio.motivo, servicio_id: @cliente_servicio.servicio_id } }
    assert_redirected_to cliente_servicio_url(@cliente_servicio)
  end

  test "should destroy cliente_servicio" do
    assert_difference('ClienteServicio.count', -1) do
      delete cliente_servicio_url(@cliente_servicio)
    end

    assert_redirected_to cliente_servicios_url
  end
end
