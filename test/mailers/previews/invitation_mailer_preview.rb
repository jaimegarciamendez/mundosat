# Preview all emails at http://localhost:3000/rails/mailers/invitation_mailer
class InvitationMailerPreview < ActionMailer::Preview
  def invitation_mailer
    InvitationMailer.with(user: User.first).invitation_mailer
  end
end
