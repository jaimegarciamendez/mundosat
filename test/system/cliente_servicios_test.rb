require "application_system_test_case"

class ClienteServiciosTest < ApplicationSystemTestCase
  setup do
    @cliente_servicio = cliente_servicios(:one)
  end

  test "visiting the index" do
    visit cliente_servicios_url
    assert_selector "h1", text: "Cliente Servicios"
  end

  test "creating a Cliente servicio" do
    visit cliente_servicios_url
    click_on "New Cliente Servicio"

    fill_in "Cliente", with: @cliente_servicio.cliente_id
    fill_in "Direccion", with: @cliente_servicio.direccion
    fill_in "Estado", with: @cliente_servicio.estado
    fill_in "Fecha alta", with: @cliente_servicio.fecha_alta
    fill_in "Fecha baja", with: @cliente_servicio.fecha_baja
    fill_in "Motivo", with: @cliente_servicio.motivo
    fill_in "Servicio", with: @cliente_servicio.servicio_id
    click_on "Create Cliente servicio"

    assert_text "Cliente servicio was successfully created"
    click_on "Back"
  end

  test "updating a Cliente servicio" do
    visit cliente_servicios_url
    click_on "Edit", match: :first

    fill_in "Cliente", with: @cliente_servicio.cliente_id
    fill_in "Direccion", with: @cliente_servicio.direccion
    fill_in "Estado", with: @cliente_servicio.estado
    fill_in "Fecha alta", with: @cliente_servicio.fecha_alta
    fill_in "Fecha baja", with: @cliente_servicio.fecha_baja
    fill_in "Motivo", with: @cliente_servicio.motivo
    fill_in "Servicio", with: @cliente_servicio.servicio_id
    click_on "Update Cliente servicio"

    assert_text "Cliente servicio was successfully updated"
    click_on "Back"
  end

  test "destroying a Cliente servicio" do
    visit cliente_servicios_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Cliente servicio was successfully destroyed"
  end
end
