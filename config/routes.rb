Rails.application.routes.draw do
    
  resources :chapters, only: [:index, :show] do
    resources :questions, only: [:show, :update] do
    end
  end
 
  resources :cliente_servicios do 
    member do
      post :bajar
      post :revivir
    end
  end

  resources :articles
  resources :categories

  resources :tickets do
    member do
      post :change_state
      post :close_state
    end
    resources :comments, only: [:create, :destroy, :update]
  end

  resources :clientes, :programs, :people, :derivations
  resources :subjects, :referents, :organizations, :pins
  resources :ipp_cases, :servicios, :images
 
  devise_for :users,
  :path => "usuarios",
  :path_names => {
    :sign_in => 'login',
    :sign_out => 'logout',
    :password => 'secret',
    :confirmation => 'verification',
    :unlock => 'unblock',
    :registration => 'register',
    :sign_up => 'cmon_let_me_in' }
  
  
  root 'static_pages#home'
  get '/ayuda', to: 'static_pages#ayuda'
  get '/contacto', to: 'static_pages#contacto'
    
  resources :subjects do
    resources :derivations
  end
  
    
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
 
end
