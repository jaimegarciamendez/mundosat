json.extract! cliente, :id, :name, :document_type, :document_number, :address, :phone, :province, :state, :postal_code, :contact_name, :contact_mail, :billing_mail, :created_at, :updated_at
json.url cliente_url(cliente, format: :json)
