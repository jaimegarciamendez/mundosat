json.extract! cliente_servicio, :id, :fecha_alta, :fecha_baja, :estado, :motivo, :direccion, :cliente_id, :servicio_id, :created_at, :updated_at
json.url cliente_servicio_url(cliente_servicio, format: :json)
