json.extract! ticket, :id, :title, :fecha_inicio, :state, :reason, :description, :created_at, :updated_at
json.url ticket_url(ticket, format: :json)
