class AppMailer < ApplicationMailer
  
  default from: 'notifications@example.com'
 
  def login_email
    @user = params[:user]
    @url  = 'http://mundosat.com/login'
    mail(to: @user.email, subject: 'Welcome to Mundo Satelital')
  end
end
