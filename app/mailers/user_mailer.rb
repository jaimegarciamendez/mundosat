class UserMailer < ApplicationMailer

  default from: 'info@mundosatelital.com.ar'
 
  def welcome_email
    @user = params[:user]
    @url  = 'http://mundosatelital.herokuapp.com/usuarios/login'
    mail(to: @user.email, subject: 'Bienvenido a la App de Mundo Satelital')
  end
end
