class InvitationMailer < ApplicationMailer
  
  default from: 'notifications@example.com'
 
  def invitation_mailer
    @user = params[:user]
    @url  = 'http://mundosat.com/login'
    mail(to: 'usuario@mail.com', subject: 'Welcome to Mundo Satelital')
  end
end
