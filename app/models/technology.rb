# == Schema Information
#
# Table name: technologies
#
#  id          :integer          not null, primary key
#  name        :string
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  provider_id :integer
#

class Technology < ApplicationRecord
	belongs_to :provider
	has_many :servicios, dependent: :destroy
end
