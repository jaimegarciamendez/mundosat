# == Schema Information
#
# Table name: images
#
#  id            :integer          not null, primary key
#  description   :string
#  likes_counter :integer
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#

class Image < ApplicationRecord

	has_one_attached :picture
		

end
