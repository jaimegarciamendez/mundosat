# == Schema Information
#
# Table name: tickets
#
#  id                  :integer          not null, primary key
#  title               :string
#  fecha_inicio        :datetime
#  reason              :string
#  description         :string
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#  cliente_servicio_id :integer
#  user_id             :integer
#  aasm_state          :string
#  state               :string
#

class Ticket < ApplicationRecord
	
	belongs_to :user
	has_many :comments, dependent: :delete_all
	validates :title, :reason, :description, presence: true

	include AASM

	aasm do
		state :Pendiente, initial: true
		state :Resolviendo, :Resuelto, :Cerrado
		
		event :progress do
			transitions from: :Pendiente, to: :Resolviendo
		end

		event :solution do
			transitions from: [:Pendiente, :Resolviendo], to: :Resuelto
		end

		event :close do
			transitions from: [:Pendiente, :Resolviendo, :Resuelto], to: :Cerrado
		end
  	end

  	#scope :creados, -> { where(aasm_state: "pending" ) }
  	#scope :en_proceso, -> { where(aasm_state: "in_progress" ) }
	#scope :ultimos, -> { order("created_at DESC").limit(3) }

end
