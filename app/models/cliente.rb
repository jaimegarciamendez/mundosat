# == Schema Information
#
# Table name: clientes
#
#  id              :integer          not null, primary key
#  name            :string
#  document_type   :string
#  document_number :string
#  address         :string
#  phone           :string
#  province        :string
#  state           :string
#  postal_code     :string
#  contact_name    :string
#  contact_mail    :string
#  billing_mail    :string
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  partido         :string
#

class Cliente < ApplicationRecord
	validates :name, :document_type, :document_number, presence: true

	validates :phone,:presence => true,
                       :length => { :minimum => 8, :maximum => 17 }

	has_many :cliente_servicios
	has_many :servicios, through: :cliente_servicios
	
end
