module PermissionsConcern
	extend ActiveSupport::Concern
	def is_normal_user?
		self.nivel_de_permiso >= 1
	end

	def is_editor?
		self.nivel_de_permiso >= 2
	end

	def is_admin?
		self.nivel_de_permiso >= 3
	end
end
	
