# == Schema Information
#
# Table name: servicios
#
#  id            :integer          not null, primary key
#  contencion    :text
#  comodato      :text
#  bajada        :integer
#  subida        :integer
#  cuota_trafico :integer
#  precio        :integer
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#  technology_id :integer
#  provider_id   :integer
#

class Servicio < ApplicationRecord
     validates :contencion, presence: true
     validates :comodato,  presence: true
     validates :bajada, presence: true
     validates :subida,  presence: true
     validates :cuota_trafico, presence: true
     validates :precio,  presence: true

     belongs_to :provider
     belongs_to :technology

     has_many :cliente_servicios
	 has_many :clientes, through: :cliente_servicios
  
 end
