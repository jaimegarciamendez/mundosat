# == Schema Information
#
# Table name: cliente_servicios
#
#  id          :integer          not null, primary key
#  fecha_alta  :datetime
#  fecha_baja  :datetime
#  aasm_state  :string
#  motivo      :string
#  direccion   :string
#  cliente_id  :integer
#  servicio_id :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  cpaid       :string
#

class ClienteServicio < ApplicationRecord
  belongs_to :cliente
  belongs_to :servicio
  
  include AASM

	aasm do

		state :Alta, initial: true
		state :Baja
		
		event :bajar do
			transitions from: :Alta, to: :Baja
		end

		event :revivir do
			transitions from: :Baja, to: :Alta
		end
   
  	end
end
