class TicketsController < ApplicationController
  before_action :set_ticket, only: [:show, :edit, :update, :destroy]
  before_action :set_cliente_servicio, only: [:new]
  before_action :authenticate_user!

  # GET /tickets
  # GET /tickets.json
  def index
    @tickets = Ticket.where.not(aasm_state: "Cerrado")
  end

  # GET /tickets/1
  # GET /tickets/1.json
  def show
    @comment = Comment.new
  end

  # GET /tickets/new
  def new
    @ticket = Ticket.new
  end

  # GET /tickets/1/edit
  def edit
  end

  # POST /tickets
  # POST /tickets.json
  def create
    @ticket = current_user.tickets.new(ticket_params)
    
    respond_to do |format|
      if @ticket.save
        format.html { redirect_to @ticket, notice: 'Ticket Creado.' }
        format.json { render :show, status: :created, location: @ticket }
      else
        format.html { render :new }
        format.json { render json: @ticket.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /tickets/1
  # PATCH/PUT /tickets/1.json
  def update
    respond_to do |format|
      if @ticket.update(ticket_params)
        format.html { redirect_to @ticket, notice: 'Ticket Actualizado.' }
        format.json { render :show, status: :ok, location: @ticket }
      else
        format.html { render :edit }
        format.json { render json: @ticket.errors, status: :unprocessable_entity }
      end
    end
  end

  def change_state
    @ticket = current_user.tickets.find(params[:id])
    if params[:tipo] == "Resolviendo"
      @ticket.progress!
    end
    if params[:tipo] == "Resuelto"
      @ticket.solution!
    end
    redirect_to tickets_path
  end

  def close_state 
    @ticket = current_user.tickets.find(params[:id])
    @ticket.close!
    redirect_to tickets_path
  end
 
  # DELETE /tickets/1
  # DELETE /tickets/1.json
  def destroy
    @ticket.destroy
    respond_to do |format|
      format.html { redirect_to tickets_url, notice: 'Ticket Eliminado.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ticket
      @ticket = Ticket.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def ticket_params
      params.require(:ticket).permit(:title, :fecha_inicio, :aasm_state,
                     :reason, :description, :cliente_servicio_id, :user_id)
    end

    def set_cliente_servicio
      @cliente_servicio = ClienteServicio.all
    end
end
