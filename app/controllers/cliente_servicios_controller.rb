class ClienteServiciosController < ApplicationController
  before_action :set_cliente_servicio, only: [:show, :edit, :update, :destroy, :bajar, :revivir]
  before_action :set_estado, only: [:index, :new]
  before_action :authenticate_user!

  # GET /cliente_servicios
  # GET /cliente_servicios.json
  def index
    @cliente_servicios = ClienteServicio.where aasm_state: @estado
  end

  # GET /cliente_servicios/1
  # GET /cliente_servicios/1.json
  def show
  end

  # GET /cliente_servicios/new
  def new
    @cliente_servicio = ClienteServicio.new
  end

  # GET /cliente_servicios/1/edit
  def edit
  end

  # POST /cliente_servicios
  # POST /cliente_servicios.json
  def create
    @cliente_servicio = ClienteServicio.new(cliente_servicio_params)

    respond_to do |format|
      if @cliente_servicio.save
        format.html { redirect_to @cliente_servicio, notice: 'Cliente servicio was successfully created.' }
        format.json { render :show, status: :created, location: @cliente_servicio }
      else
        format.html { render :new }
        format.json { render json: @cliente_servicio.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /cliente_servicios/1
  # PATCH/PUT /cliente_servicios/1.json
  def update
    respond_to do |format|
      if @cliente_servicio.update(cliente_servicio_params)
        format.html { redirect_to @cliente_servicio, notice: 'Cliente servicio was successfully updated.' }
        format.json { render :show, status: :ok, location: @cliente_servicio }
      else
        format.html { render :edit }
        format.json { render json: @cliente_servicio.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /cliente_servicios/1
  # DELETE /cliente_servicios/1.json
  def destroy
    @cliente_servicio.destroy
    respond_to do |format|
      format.html { redirect_to cliente_servicios_url, notice: 'Cliente servicio was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def bajar
    destino = "Baja"
    if @cliente_servicio.bajar
      @cliente_servicio.update(fecha_baja: Time.zone.now)
      flash[:notice] = "Servicio dado de baja exitosamente"
    else
      destino = "Alta"
      flash[:danger] = "Error al dar de baja el servicio"
    end
    redirect_to cliente_servicios_path(estado: destino)
  end

  def revivir
    if @cliente_servicio.revivir
      @cliente_servicio.update(fecha_baja: nil)
      flash[:notice] = "Servicio revivido con éxito"
    end
    redirect_to cliente_servicios_path(estado: "Alta")
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_cliente_servicio
      @cliente_servicio = ClienteServicio.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def cliente_servicio_params
      params.require(:cliente_servicio).permit(:fecha_alta, :fecha_baja, :estado, :motivo, :direccion, :cliente_id, :servicio_id)
    end

    def set_estado
      #pasamos como paramétro el estado (alta o baja)
      @estado = params[:estado]
    end

end
