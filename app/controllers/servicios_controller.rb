class ServiciosController < ApplicationController
before_action :set_servicio, only: [:show,:edit,:update,:destroy]
before_action :set_provider, only: [:index, :new]
before_action :authenticate_user!

def index
   @servicios = Servicio.where provider_id: @provider
end

def new
    @tecnologias = Technology.where provider_id: @provider
    @servicio = Servicio.new
end

def create
   @servicio = Servicio.new(servicio_params)
        if @servicio.save
            redirect_to @servicio, notice: 'Servicio creado'
        else
          redirect_to servicios_path( :provider_name=>"#{@servicio.provider.name}" ),
          notice: 'Error: no se pudo crear'
        end
end


def edit
end

def show
end

def update
    if @servicio.update(servicio_params)
      redirect_to @servicio, notice: 'Servicio actualizado'
    else
      redirect_to servicios_path( :provider_name=>"#{@servicio.provider.name}" ),
          notice: 'Error: no se pudo actualizar'
    end
  end

def destroy
    if @servicio.destroy
      redirect_to servicios_path( :provider_name=>"#{@servicio.provider.name}" ),
      notice: 'Servicio eliminado'
    else
      redirect_to servicios_path( :provider_name=>"#{@servicio.provider.name}" ),
      notice: 'Error: no se pudo eliminar'
    end
end

private

    def set_servicio
      @servicio = Servicio.find params[:id]
    end

    def servicio_params
      params.require(:servicio).permit(:contencion, :comodato,
                                       :bajada, :subida, :cuota_trafico,
                                       :precio, :technology_id, :provider_id)
    end

    def set_provider
      #pasamos como paramétro el nombre del proveedor
      @provider = Provider.find_by(name: params[:provider_name]) 
    end


end
