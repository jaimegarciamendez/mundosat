class ImagesController < ApplicationController
    before_action :set_image, only: [:show,:edit,:update,:destroy]
    before_action :authenticate_user!
    
    def index
        @images = Image.all
    end
    
    def create
        @image = Image.new(image_params)
        if @image.save
            #flash[:notice] = "Imagen creada"
            redirect_to @image, notice: 'Imagen creada'
        end
    end
        
    def new
        @image = Image.new
    end    
    
    def edit
    end
    
    def show
    end
    
    def update
         if @image.update(image_params)
            redirect_to images_path, notice: 'Imágen actualizada'
            end
    end
        
    def destroy
        @image.destroy
        redirect_to images_path, :notice => "Imágen eliminada"
    end
    
    private
    
    def image_params
        params.require(:image).permit(:description, :likes_counter, :picture)
    end
    
    def set_image
        @image = Image.find params[:id]
    end
    
end
