# == Schema Information
#
# Table name: cliente_servicios
#
#  id          :integer          not null, primary key
#  fecha_alta  :datetime
#  fecha_baja  :datetime
#  aasm_state  :string
#  motivo      :string
#  direccion   :string
#  cliente_id  :integer
#  servicio_id :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  cpaid       :string
#

class ClienteServicioSerializer < ActiveModel::Serializer
  attributes :id, :fecha_alta, :fecha_baja, :estado, :motivo, :direccion
  has_one :cliente
  has_one :servicio
end
