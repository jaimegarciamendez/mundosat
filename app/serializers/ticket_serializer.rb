# == Schema Information
#
# Table name: tickets
#
#  id                  :integer          not null, primary key
#  title               :string
#  fecha_inicio        :datetime
#  reason              :string
#  description         :string
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#  cliente_servicio_id :integer
#  user_id             :integer
#  aasm_state          :string
#  state               :string
#

class TicketSerializer < ActiveModel::Serializer
  attributes :id, :title, :fecha_inicio, :state, :reason, :description
end
