# == Schema Information
#
# Table name: clientes
#
#  id              :integer          not null, primary key
#  name            :string
#  document_type   :string
#  document_number :string
#  address         :string
#  phone           :string
#  province        :string
#  state           :string
#  postal_code     :string
#  contact_name    :string
#  contact_mail    :string
#  billing_mail    :string
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  partido         :string
#

class ClienteSerializer < ActiveModel::Serializer
  attributes :id, :name, :document_type, :document_number, :address, :phone, :province, :state, :postal_code, :contact_name, :contact_mail, :billing_mail
end
