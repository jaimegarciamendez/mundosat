module ApplicationHelper
    
    #Returns a full title
    def full_title(page_title = '')
        base_title = "Portal de Mundo Satelital"
        
        if page_title.empty?
            base_title
        else
            page_title + " | " + base_title
        end
    end
    
end
