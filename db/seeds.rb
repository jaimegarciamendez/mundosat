
Cliente.create!([
  {name: "Jaime Garcia Mendez", document_type: "DNI", document_number: "31060702", address: "Calle 253 1275", phone: "116-268-8950", province: "Buenos Aires", state: "Sourigues", postal_code: "1837", contact_name: "Jaime", contact_mail: "jaime@mundosatelital.com.ar"},
  {name: "Nadia Cudina", document_type: "DNI", document_number: "32758102", address: "Calle 253 1275", phone: "1162105044", province: "Bs As", state: "Sourigues", postal_code: "1837", contact_name: "Nadia", contact_mail: "nadiacudina@gmail.com"}  
])

Provider.create!([
  {name: "Raasat", phone: "0800-888-9999"},
  {name: "Ufinet", phone: "4520-5555"},
  {name: "Telespazio", phone: "0800-XXX-XXXX"},
  {name: "Movistar", phone: "0800-999-6666"},
  {name: "Telefónica", phone: "0800-XXX-XXXX"},
  {name: "Servicio Satelital", phone: "0800-XXX-XXXX"},
  {name: "Orbith", phone: "0800-XXX-XXXX"}
])

Technology.create!([
  {name: "Gilat Gemini ii-C", provider_id: 1},
  {name: "Shiron", provider_id: 1},
  {name: "Internet Wireless", provider_id: 2},
  {name: "MPLS Wireless", provider_id: 2},
  {name: "iDirect", provider_id: 3},
  {name: "4G Wireless", provider_id: 4},
  {name: "Gilat Gemini ii-C", provider_id: 5},
  {name: "Gilat Gemini ii-C", provider_id: 6},
  {name: "Newtec 2210", provider_id: 7},
  {name: "Newtec 3310", provider_id: 7}
])

Servicio.create!([
  {contencion: "Cir 100%", comodato: "Si", bajada: 2048, subida: 2, cuota_trafico: 0, precio: 499, technology_id: 5, provider_id: 3},
  {contencion: "Cir 100%", comodato: "Si", bajada: 450, subida: 64, cuota_trafico: 0, precio: 300, technology_id: 5, provider_id: 3},
  {contencion: "Cir 100%", comodato: "Si", bajada: 1024, subida: 256, cuota_trafico: 25, precio: 250, technology_id: 1, provider_id: 1},
  {contencion: "1:15", comodato: "Si", bajada: 2048, subida: 512, cuota_trafico: 200, precio: 400, technology_id: 1, provider_id: 1},
  {contencion: "1:1", comodato: "Si", bajada: 2048, subida: 2048, cuota_trafico: 0, precio: 300, technology_id: 4, provider_id: 2},
  {contencion: "1:1", comodato: "Si", bajada: 2048, subida: 2048, cuota_trafico: 0, precio: 250, technology_id: 3, provider_id: 2},
  {contencion: "Cir 100%", comodato: "Si", bajada: 0, subida: 0, cuota_trafico: 30, precio: 32, technology_id: 6, provider_id: 4},
  {contencion: "1:10", comodato: "Si", bajada: 1024, subida: 256, cuota_trafico: 0, precio: 200, technology_id: 7, provider_id: 5},
  {contencion: "Cir 100%", comodato: "Si", bajada: 30000, subida: 5000, cuota_trafico: 40, precio: 180, technology_id: 9, provider_id: 7},
  {contencion: "Cir 100%", comodato: "Si", bajada: 30000, subida: 5000, cuota_trafico: 70, precio: 250, technology_id: 9, provider_id: 7},
  {contencion: "Cir 100%", comodato: "Si", bajada: 30000, subida: 5000, cuota_trafico: 120, precio: 350, technology_id: 9, provider_id: 7},
  {contencion: "Cir 100%", comodato: "Si", bajada: 30000, subida: 5000, cuota_trafico: 200, precio: 500, technology_id: 9, provider_id: 7},
  {contencion: "Cir 100%", comodato: "Si", bajada: 20000, subida: 20000, cuota_trafico: 0, precio: 600, technology_id: 10, provider_id: 7},
  {contencion: "Cir 100%", comodato: "Si", bajada: 0, subida: 0, cuota_trafico: 15, precio: 23, technology_id: 6, provider_id: 4},
  {contencion: "1:4", comodato: "Si", bajada: 512, subida: 128, cuota_trafico: 70, precio: 450, technology_id: 1, provider_id: 1},
  {contencion: "1:4", comodato: "Si", bajada: 768, subida: 384, cuota_trafico: 90, precio: 520, technology_id: 1, provider_id: 1},
  {contencion: "Cir 100%", comodato: "Si", bajada: 5120, subida: 5120, cuota_trafico: 0, precio: 350, technology_id: 4, provider_id: 2},
  {contencion: "1:16", comodato: "Si", bajada: 256, subida: 128, cuota_trafico: 0, precio: 345, technology_id: 8, provider_id: 6},
  {contencion: "1:16", comodato: "Si", bajada: 384, subida: 384, cuota_trafico: 0, precio: 432, technology_id: 8, provider_id: 6},
  {contencion: "1:16", comodato: "Si", bajada: 512, subida: 256, cuota_trafico: 0, precio: 411, technology_id: 8, provider_id: 6},
  {contencion: "1:16", comodato: "Si", bajada: 512, subida: 512, cuota_trafico: 0, precio: 476, technology_id: 8, provider_id: 6},
  {contencion: "1:16", comodato: "Si", bajada: 1024, subida: 128, cuota_trafico: 0, precio: 443, technology_id: 8, provider_id: 6},
  {contencion: "1:16", comodato: "Si", bajada: 1024, subida: 256, cuota_trafico: 0, precio: 476, technology_id: 8, provider_id: 6},
  {contencion: "1:16", comodato: "Si", bajada: 1024, subida: 512, cuota_trafico: 0, precio: 541, technology_id: 8, provider_id: 6},
  {contencion: "1:16", comodato: "Si", bajada: 2048, subida: 512, cuota_trafico: 0, precio: 671, technology_id: 8, provider_id: 6},
  {contencion: "1:16", comodato: "Si", bajada: 2048, subida: 2048, cuota_trafico: 0, precio: 1063, technology_id: 8, provider_id: 6}
])
