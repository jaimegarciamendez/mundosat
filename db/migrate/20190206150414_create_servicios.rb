class CreateServicios < ActiveRecord::Migration[5.1]
  def change
    create_table :servicios do |t|
      t.text :tecnologia
      t.text :contencion
      t.text :comodato
      t.integer :bajada
      t.integer :subida
      t.integer :cuota_trafico
      t.integer :precio

      t.timestamps
    end
  end
end
