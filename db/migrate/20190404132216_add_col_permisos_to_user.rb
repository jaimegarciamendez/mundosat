class AddColPermisosToUser < ActiveRecord::Migration[5.2]
  def change
  	add_column :users, :nivel_de_permiso, :integer, default: 1
  end
end
