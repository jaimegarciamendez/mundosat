class AddProviderIdToServicios < ActiveRecord::Migration[5.2]
  def change
    add_reference :servicios, :provider, foreign_key: true
  end
end
