class CreateTickets < ActiveRecord::Migration[5.2]
  def change
    create_table :tickets do |t|
      t.string :title
      t.datetime :fecha_inicio
      t.string :cliente_servicio_id
      t.string :aasm_state
      t.string :reason
      t.string :description
      t.references :user, foreign_key: true
      t.timestamps
    end
  end
end
