class AddProviderIdToTechnologies < ActiveRecord::Migration[5.2]
  def change
    add_column :technologies, :provider_id, :integer
  end
end
