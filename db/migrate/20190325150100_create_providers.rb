class CreateProviders < ActiveRecord::Migration[5.2]
  def change
    create_table :providers do |t|
      t.string :name
      t.string :phone
      t.string :technology

      t.timestamps
    end
  end
end
