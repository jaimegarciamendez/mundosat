class AddProviderIdToTechnology < ActiveRecord::Migration[5.2]
  def change
    add_reference :technologies, :provider, foreign_key: true
  end
end
