class AddTechnologyIdToServicios < ActiveRecord::Migration[5.2]
  def change
    add_reference :servicios, :technology, foreign_key: true
  end
end
