class CreateUserAnswers < ActiveRecord::Migration[5.2]
  def change
    create_table :user_answers do |t|
      t.references :user
      t.references :question
      t.integer :chosen_option

      t.timestamps
    end
  end
end
