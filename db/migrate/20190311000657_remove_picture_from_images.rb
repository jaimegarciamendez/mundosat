class RemovePictureFromImages < ActiveRecord::Migration[5.2]
  def change
    remove_column :images, :picture, :string
  end
end
