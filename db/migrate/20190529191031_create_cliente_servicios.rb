class CreateClienteServicios < ActiveRecord::Migration[5.2]
  def change
    create_table :cliente_servicios do |t|
      t.datetime :fecha_alta
      t.datetime :fecha_baja
      t.string :aasm_state
      t.string :motivo
      t.string :direccion
      t.references :cliente, foreign_key: true
      t.references :servicio, foreign_key: true

      t.timestamps
    end
  end
end
