class CreateClientes < ActiveRecord::Migration[5.2]
  def change
    create_table :clientes do |t|
      t.string :name
      t.string :document_type
      t.string :document_number
      t.string :address
      t.string :phone
      t.string :province
      t.string :partido
      t.string :state
      t.string :postal_code
      t.string :contact_name
      t.string :contact_mail

      t.timestamps
    end
  end
end
