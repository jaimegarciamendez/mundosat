class RemovePositionFromServicios < ActiveRecord::Migration[5.2]
  def change
    remove_column :servicios, :position, :text
  end
end
