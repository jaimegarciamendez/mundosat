class RenameTechCol < ActiveRecord::Migration[5.2]
  def self.up
    rename_column :servicios, :tecnologia, :position
  end
end
