# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_06_26_210142) do

  create_table "active_storage_attachments", force: :cascade do |t|
    t.string "name", null: false
    t.string "record_type", null: false
    t.integer "record_id", null: false
    t.integer "blob_id", null: false
    t.datetime "created_at", null: false
    t.index ["blob_id"], name: "index_active_storage_attachments_on_blob_id"
    t.index ["record_type", "record_id", "name", "blob_id"], name: "index_active_storage_attachments_uniqueness", unique: true
  end

  create_table "active_storage_blobs", force: :cascade do |t|
    t.string "key", null: false
    t.string "filename", null: false
    t.string "content_type"
    t.text "metadata"
    t.bigint "byte_size", null: false
    t.string "checksum", null: false
    t.datetime "created_at", null: false
    t.index ["key"], name: "index_active_storage_blobs_on_key", unique: true
  end

  create_table "articles", force: :cascade do |t|
    t.string "title"
    t.string "body"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "state"
    t.string "aasm_state"
  end

  create_table "categories", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "chapters", force: :cascade do |t|
    t.integer "number"
    t.string "title"
    t.string "video_name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "cliente_servicios", force: :cascade do |t|
    t.datetime "fecha_alta"
    t.datetime "fecha_baja"
    t.string "aasm_state"
    t.string "motivo"
    t.string "direccion"
    t.integer "cliente_id"
    t.integer "servicio_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "cpaid"
    t.index ["cliente_id"], name: "index_cliente_servicios_on_cliente_id"
    t.index ["servicio_id"], name: "index_cliente_servicios_on_servicio_id"
  end

  create_table "clientes", force: :cascade do |t|
    t.string "name"
    t.string "document_type"
    t.string "document_number"
    t.string "address"
    t.string "phone"
    t.string "province"
    t.string "state"
    t.string "postal_code"
    t.string "contact_name"
    t.string "contact_mail"
    t.string "billing_mail"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "partido"
  end

  create_table "comments", force: :cascade do |t|
    t.integer "user_id"
    t.integer "ticket_id"
    t.text "body"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["ticket_id"], name: "index_comments_on_ticket_id"
    t.index ["user_id"], name: "index_comments_on_user_id"
  end

  create_table "derivations", force: :cascade do |t|
    t.integer "subject_id"
    t.date "date_from"
    t.string "reason"
    t.string "state"
    t.date "next_control"
    t.string "derivable_type"
    t.integer "derivable_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["derivable_type", "derivable_id"], name: "index_derivations_on_derivable_type_and_derivable_id"
    t.index ["subject_id"], name: "index_derivations_on_subject_id"
  end

  create_table "has_categories", force: :cascade do |t|
    t.integer "article_id"
    t.integer "category_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["article_id"], name: "index_has_categories_on_article_id"
    t.index ["category_id"], name: "index_has_categories_on_category_id"
  end

  create_table "images", force: :cascade do |t|
    t.string "description"
    t.integer "likes_counter"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "ipp_cases", force: :cascade do |t|
    t.string "ipp_number"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["ipp_number"], name: "index_ipp_cases_on_ipp_number", unique: true
  end

  create_table "organizations", force: :cascade do |t|
    t.string "name"
    t.string "ciudad"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "people", force: :cascade do |t|
    t.string "name"
    t.integer "dni"
    t.string "nacionalidad"
    t.integer "edad"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "pins", force: :cascade do |t|
    t.string "title"
    t.text "caption"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "programs", force: :cascade do |t|
    t.string "name"
    t.integer "duracion"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "providers", force: :cascade do |t|
    t.string "name"
    t.string "phone"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "questions", force: :cascade do |t|
    t.integer "chapter_id"
    t.text "question"
    t.text "answers"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "correct_answer"
    t.integer "question_number"
    t.index ["chapter_id"], name: "index_questions_on_chapter_id"
  end

  create_table "referents", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "servicios", force: :cascade do |t|
    t.text "contencion"
    t.text "comodato"
    t.integer "bajada"
    t.integer "subida"
    t.integer "cuota_trafico"
    t.integer "precio"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "technology_id"
    t.integer "provider_id"
    t.index ["provider_id"], name: "index_servicios_on_provider_id"
    t.index ["technology_id"], name: "index_servicios_on_technology_id"
  end

  create_table "subjects", force: :cascade do |t|
    t.integer "ipp_case_id"
    t.integer "person_id"
    t.string "role"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["ipp_case_id", "person_id"], name: "index_subjects_on_ipp_case_id_and_person_id", unique: true
    t.index ["ipp_case_id"], name: "index_subjects_on_ipp_case_id"
    t.index ["person_id"], name: "index_subjects_on_person_id"
  end

  create_table "technologies", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "provider_id"
    t.index ["provider_id"], name: "index_technologies_on_provider_id"
  end

  create_table "tickets", force: :cascade do |t|
    t.string "title"
    t.datetime "fecha_inicio"
    t.string "reason"
    t.string "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "cliente_servicio_id"
    t.integer "user_id"
    t.string "aasm_state"
    t.string "state"
    t.index ["user_id"], name: "index_tickets_on_user_id"
  end

  create_table "user_answers", force: :cascade do |t|
    t.integer "user_id"
    t.integer "question_id"
    t.integer "chosen_option"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["question_id"], name: "index_user_answers_on_question_id"
    t.index ["user_id"], name: "index_user_answers_on_user_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string "current_sign_in_ip"
    t.string "last_sign_in_ip"
    t.string "name"
    t.string "permission_level"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "permission_nivel"
    t.integer "nivel_de_permiso", default: 1
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  add_foreign_key "active_storage_attachments", "active_storage_blobs", column: "blob_id"
  add_foreign_key "comments", "tickets"
  add_foreign_key "comments", "users"
  add_foreign_key "derivations", "subjects"
  add_foreign_key "has_categories", "articles"
  add_foreign_key "has_categories", "categories"
  add_foreign_key "subjects", "ipp_cases"
  add_foreign_key "subjects", "people"
end
